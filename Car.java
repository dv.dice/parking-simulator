import java.util.Random;
public class Car {

    private int parkingTime;
    private int number;

    public String toString(){
        return "Car №" + this.number + ". " + this.parkingTime + " moves left for parking";
    }


    public Car(int number, int parkingTime){
        this.number = number;
        this.parkingTime = parkingTime;
    }

    public int getNumber() {
        return number;
    }

    public int getParkingTime() {
        return parkingTime;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setParkingTime(int parkingTime) {
        this.parkingTime = parkingTime;
    }
}
