import java.util.Random;
import java.util.Scanner;

public class Control {
    private boolean isOn;

    public Control(){
        this.isOn = true;
    }

    public void setOn(boolean on) {
        isOn = on;
    }

    public boolean isOn() {
        return isOn;
    }

    public void help(){
        System.out.println("COMMANDS:");
        System.out.println("/help - available commands");
        System.out.println("/next - next move");
        System.out.println("/off - off the machine");
        System.out.println("/clean - cleans the parking zone");
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        Control control = new Control();

        System.out.println("How many parking spaces do you need?");
        int amount = scanner.nextInt();
        ParkingZone parkingZone = new ParkingZone(amount);
        int number = 1000;
        System.out.println("/help to get the commands");
        while (control.isOn){
            switch (scanner.nextLine()){
                case "/help":{
                    control.help();
                    break;
                }
                case "/clean":{
                    parkingZone.cleaningService();
                    System.out.println("Cleaned");
                }
                case "/off":{
                    control.setOn(false);
                }
                case "/next":{

                    if (parkingZone.isThereAvailablePlace()){
                        int parkingTime = random.nextInt(9) + 1;
                        Car car = new Car(number, parkingTime);
                        ParkingZone.takePlace(car, parkingTime);
                        System.out.println("A car gets the place.");
                        System.out.println(car.toString());
                    }else{
                        System.out.println(" There are no available places.");
                    }
                    number++;
                    break;
                }
            }
        }
    }
}
