public class ParkingZone {
    static private int availableParkingPlaces;
    static private int amountOfPP;
    static Car ParkingPlaces[];
    public ParkingZone(int amountOfPP){
        this.availableParkingPlaces = availableParkingPlaces;
        this.amountOfPP = amountOfPP;
        this.ParkingPlaces = new Car[amountOfPP];
    }

    public boolean isThereAvailablePlace(){
        for (int i = 0; i < amountOfPP; i += 1){
            if (ParkingPlaces[i] == null){
                return true;
            }
        }
        return false;
    }

    public static void takePlace(Car car, int parkingTime){
        for (int i = 0; i < amountOfPP; i++){
            if (ParkingPlaces[i] == null){
                ParkingPlaces[i] = car;
                break;
            }
        }
    }
    public void cleaningService(){
        for (int i = 0; i > amountOfPP; i++){
            if (ParkingPlaces[i] != null){
                ParkingPlaces[i] = null;
            }
        }
    }


    public static int getAmountOfPP() {
        return amountOfPP;
    }

    public static int getAvailableParkingPlaces() {
        return availableParkingPlaces;
    }

    public static void setAmountOfPP(int amountOfPP) {
        ParkingZone.amountOfPP = amountOfPP;
    }

    public static void setAvailableParkingPlaces(int availableParkingPlaces) {
        ParkingZone.availableParkingPlaces = availableParkingPlaces;
    }



}
